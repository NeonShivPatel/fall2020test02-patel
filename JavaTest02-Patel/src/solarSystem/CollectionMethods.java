//@author: Shiv Patel
//@ID: 1935098
package solarSystem;

import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> innerPlanets = null;
		//Loop goes through the inputed planets and will only add the inner planets to the new Collection<Planet>
		for (Planet e: planets) {
			if (e.getOrder() == 1 || e.getOrder() == 2 || e.getOrder() == 3) {
				innerPlanets.add(e);
			}
			else {
			}
		}//end loop
		return innerPlanets;
	}
	
}
