package solarSystem;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	@Override
	public boolean equals(Object obj) {
		//Make sure that the object inputed is a Planet object and not anything else
		if(!(obj instanceof Planet)) {
			return false;
		}
		//If it is a Planet object we proceed to see if they are equal or not
		else {
			Planet two = (Planet)obj;
			if (this.name.equals(two.getName()) && this.order == two.getOrder() ) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	
	@Override
	public int hashCode() {
		String combined = this.name + this.order; 
		return combined.hashCode();
	}

	@Override
	public int compareTo(Planet o) {
		//if statement checks whether or not the two objects have the same name and goes through different if statements depending on the answer
		if (this.name.equals(o.getName())) {
			//if-else statement checks whether the inputed object is the same, or comes before/after "this" end returns a different answer
			if (this.planetarySystemName.hashCode() == o.getPlanetarySystemName().hashCode()) {
				return 0;
			}
			else if (this.planetarySystemName.hashCode() < o.getPlanetarySystemName().hashCode()) {
				return -1;
			}
			else {
				return 1;
			}
		}
		else {
			//if statement checks whether the o object comes before or after the "this" object
			if (this.name.hashCode() < o.getName().hashCode()) {
				return -1;
			}
			else {
				return 1;
			}
		}
	}
	
	
	
}
