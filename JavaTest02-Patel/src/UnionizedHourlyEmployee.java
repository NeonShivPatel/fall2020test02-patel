//@author: Shiv Patel
//@ID: 1935098
public class UnionizedHourlyEmployee extends HourlyEmployee {
	private int pensionContribution;
	public UnionizedHourlyEmployee(int salary, int hours, int pension) {
		super(salary,hours);
		this.pensionContribution = pension;
	}

	@Override
	public int getYearlyPay() {	
		return super.getYearlyPay()+this.pensionContribution;
	}

}
