//@author: Shiv Patel
//@ID: 1935098
public class SalariedEmployee implements Employee {
	private int yearlySalary;
	public SalariedEmployee(int salary) {
		yearlySalary=salary;
	}
	
	@Override
	public int getYearlyPay() {
		return this.yearlySalary;
	}

}
