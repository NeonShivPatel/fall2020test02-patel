//@author: Shiv Patel
//@ID: 1935098
public class HourlyEmployee implements Employee {
	private int hoursWorked;
	private int hourlySalary;
	public HourlyEmployee(int salary, int hours) {
		hourlySalary = salary;
		hoursWorked= hours;
	}
	
	@Override
	public int getYearlyPay() {	
		return this.hoursWorked*this.hourlySalary*52;
	}

}
