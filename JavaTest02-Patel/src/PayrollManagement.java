//@author: Shiv Patel
//@ID: 1935098
public class PayrollManagement {
	
	public static void main(String[] args) {
		//Create Employee[] and hard code the values in each index
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(20000);
		employees[1] = new UnionizedHourlyEmployee(30, 45, 5000);
		employees[2] = new HourlyEmployee(40, 40);
		employees[3] = new UnionizedHourlyEmployee(25, 40, 3500);
		employees[4] = new SalariedEmployee(21000);
		
		//Create PayrollManagement object in order to use the instance method getTotalExpenses
		PayrollManagement management = new PayrollManagement();
		int totalExpenses = management.getTotalExpenses(employees);
		System.out.println("The total amount of expenses that need to be made is " + totalExpenses + "$");
		
	}
	//Method getTotalExpenses takes as input and Employee[] and will loop through it and add up all the yearly salaries 
	public int getTotalExpenses(Employee[] e) {
		int expenses = 0;
		//For loop that gets all the yearly salaries and adds them in the int expenses
		for (int i = 0; i < e.length; i++) {
			expenses += e[i].getYearlyPay();
		}//end loop
		return expenses;
	}
}
